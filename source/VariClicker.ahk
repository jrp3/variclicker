﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

holdTime = 0 ; Tracks the amount of time user has held down the mouse button
dragFlag = 0 ; Flag -- 0: drag mode inactive, 1: drag mode active
IniRead, userConfiguredKey, config.ini, Settings, userKey ; Reads user-configured hotkey from config.ini file
IniRead, delayRight, config.ini, Settings, delayForRClick ; Reads user-configured right-click delay from config.ini file
IniRead, delayDrag, config.ini, Settings, delayForDrag ; Reads user-configured drag delay from config.ini file
Hotkey, $%userConfiguredKey%, userKey
return

userKey:
	if (dragFlag = 1)
	{
		Send {LButton Up}
		dragFlag = 0
	}
	else if (dragFlag = 0)
	{
		while GetKeyState("LButton", "P")
		{
			holdTime += 50
			Sleep, 50
		}
		if (holdTime <= delayRight)
		{
			Send {LButton}
		}
		else if (holdTime > delayRight && holdTime <= delayDrag)
		{
			Send {RButton}
		}
		else
		{
			Send {LButton Down}
			dragFlag = 1
		}
		holdTime = 0
	}
return