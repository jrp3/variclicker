﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

IniRead, userKey, config.ini, Settings, userKey ; Reads user-configured hotkey from config.ini file
IniRead, delayRight, config.ini, Settings, delayForRClick ; Reads user-configured right-click delay from config.ini file
IniRead, delayDrag, config.ini, Settings, delayForDrag ; Reads user-configured drag delay from config.ini file

Gui, Add, Text, section, What key would you like to use to control mouse clicks? (see readme.txt for allowed keys)
Gui, Add, Text, xs section, Key: 
Gui, Add, Edit, vKey ys, %userKey%
Gui, Add, Text, xs section, How long do you want to hold down the key to initiate a right-click?
Gui, Add, Text, xs section, At least 
Gui, Add, Edit, vRight ys, %delayRight%
Gui, Add, Text, ys, millisecond (1000 ms is 1 s)
Gui, Add, Text, xs section, How long do you want to hold down the key to initiate a left-click drag?
Gui, Add, Text, xs section, At least 
Gui, Add, Edit, vDrag ys, %delayDrag%
Gui, Add, Text, ys, millisecond (1000 ms is 1 s)
Gui, Add, Button, xs section Default, Save Configuration and Exit
Gui, Add, Button, ys, Save and Launch VariClicker
Gui, Show,, VariClicker Configuration
return

GuiClose:
ExitApp

ButtonSaveConfigurationandExit:
Gui, Submit
IniWrite, %Key%, config.ini, Settings, userKey
IniWrite, %Right%, config.ini, Settings, delayForRClick
IniWrite, %Drag%, config.ini, Settings, delayForDrag
ExitApp

ButtonSaveandLaunchVariClicker:
Gui, Submit
IniWrite, %Key%, config.ini, Settings, userKey
IniWrite, %Right%, config.ini, Settings, delayForRClick
IniWrite, %Drag%, config.ini, Settings, delayForDrag
Run, VariClicker.exe
ExitApp